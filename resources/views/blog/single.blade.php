@extends('main')

{{-- double quotation 을 사용하면 전달된 객체에 접근 할 수 있다. --}}
@section('title', "| $post->title")

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1> {{ $post->title }} </h1>
            <p> {{ $post->body }} </p>
            <hr>
            <p> Posted in: {{ $post->category->name }} {{ $post->category->created_at }}</p>
        </div>
    </div>
@endsection
