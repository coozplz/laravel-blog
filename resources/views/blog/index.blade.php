@extends('main')

@section('title', '| Blog')


@section('content')


    <h1> Blog </h1>
    <hr>
    <div class="row">
        <div class="col-md-8">
            @foreach ($posts as $post)
                <h3> {{ substr($post->title, 0, 50) }}
                <p> {{ substr($post->body, 0, 300) }}

                <p>
                    <a href="{{ url('blog/' . $post->slug)}}" class="btn btn-default"> Read More </a>

                </p>

            @endforeach



        </div>
    </div>
@endsection
