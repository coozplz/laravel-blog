@extends('main')

@section('title', '| Edit Tag')


@section('content')

    <div class="row">

        <h1> Edit Tag </h1>

        {!! Form::model($tag, ['route' => ['tags.update', $tag->id], 'method' => 'PUT']) !!}

        {{ Form::label('name', 'Name: ') }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}

        {{ Form::submit('Save changes', ['class' => 'btn btn-success']) }}
        {{ Form::close() }}
    </div>



@endsection
