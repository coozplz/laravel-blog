@extends('main')

@section('title', '| Contact')
@section('stylesheets')
@endsection
@section('content')
<div class="page-header">
    <h1> Contact me</h1>
    <p> Please fill in the blank... </p>
</div>


<form class="form-horizontal" role="form">

    <div class="form-group">
        <label for="name">Name :</label>
        <input type="name" class="form-control" id="name">
    </div>
    <div class="form-group">
        <label for="email">Email address :</label>
        <input type="email" class="form-control" id="email">
    </div>

    <div class="form-group">
        <label for="phone">Mobile :</label>
        <input type="phone" class="form-control" id="phone">
    </div>

    <div class="form-group">
        <label for="message"> Message : </label>
        <textarea class="form-control" row="5" id="message"> </textarea>
    </div>

    <div class="btn-group pull-right">
        <div class="btn-group">
            <button type="button" class="btn btn-primary">Submit</button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-default">Cancel</button>
        </div>
    </div>
</form>
@endsection

@section('scripts')
@endsection
