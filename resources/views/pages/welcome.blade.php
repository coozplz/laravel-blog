@extends('main')

@section('title', '| Homepage')
@section('content')


 <div class="row">
     <div class="col-md-12">
         <div class="jumbotron">
             <h1> Welcome to my blog..!!</h1>
             <p class="lead"> Thank you so much for visiting...</p>
             <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular post </a></p>
         </div>
     </div>
 </div>
 <div class="row">
     <div class="col-md-8">
         @foreach ($posts as $post)
             <div class="post">
                 <h3>{{ substr($post->title, 0, 50) }} {{ strlen($post->title) > 50 ? "..." : "" }} </h3>
                 <p> {{ substr($post->body, 0, 50) }} {{ strlen($post->body) > 50 ? "..." : "" }}

             </div>
             <div>
                 <a href="{{ url('blog/' . $post->slug) }}" class="btn btn-primary">Read more! </a>
             </div>
             <hr />
         @endforeach
     </div>
     <div class="col-md-3 col-md-offset-1">
         <h2> sidebar </h2>
     </div>
 </div>
@endsection
