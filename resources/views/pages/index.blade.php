@extends('main')


@section('title', '| All Posts')


@section('content')

    <div class="row">
        <div class="col-md-10">
            <h1> All Posts</h1>
        </div>

        <div class="col-md-2">
            <a href="{{ route('posts.create')}}" class="btn btn-lg btn-block btn-primary"> Create Post </a>
        </div>
        <div class="col-md-12">
            <hr />
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th> # </th>
                    <th> Category </th>
                    <th> Title </th>
                    <th> Body </th>
                    <th> Created At </th>
                    <th>  </th>
                </thead>

                <tbody>
                    @foreach ($posts as $post)
                        <tr>

                            <td>{{ $post->id }} </td>
                            <td> {{ $post->category->name }}
                            <td>{{ substr($post->title, 0, 50) }} {{ strlen($post->title) > 50 ? "..." : "" }} </td>
                            <td>{{ substr($post->body, 0, 250) }} {{ strlen($post->body) > 250 ? "..." : "" }} </td>
                            <td>{{ date('M j, Y', strtotime($post->created_at)) }} </td>
                            <td><a href="/posts/{{ $post->id  }}" class="btn btn-default"> View </a> <a href="/posts/{{ $post->id }}/edit" class="btn btn-default"> Edit </a> </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

            <div class="text-center">
                {!! $posts->links(); !!}
            </div>

        </div>

    </div>

@endsection
