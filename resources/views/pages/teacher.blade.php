@extends('main')

@section('title', '| Teacher')
@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1> Teacher's page</h1>
                <p class="lead"> Thank you so much for visiting...</p>
                <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular post </a></p>
            </div>
        </div>
    </div>
@endsection
