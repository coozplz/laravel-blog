@extends('main')

@section('title', '| Login')

@section('content')

    {{--
    Form Helper를 사용하지 않고 수동으로 폼을 구성하는 경우에는 반드시 csrf_field() 를 form tag안에 구성해야 한다.

    그렇지 않으면 Laravel에서는 요청을 수락하지 않는다...
    --}}


    <div class="row">

        <div class="col-md 6 col-md-offset-3">

          {!! Form::open() !!}

            {{ Form::label('email', 'Email: ') }}
            {{ Form::email('email', null, ['class' => 'form-control']) }}

            {{ Form::label('password', 'Password: ') }}
            {{ Form::password('password', ['class' => 'form-control']) }}


            <br>
            {{ Form::checkbox('remember') }} {{ Form::label('remember', 'Remember Me') }}

            <br>

            {{ Form::submit('Login', ['class' => 'btn btn-primary btn-block']) }}

            <p> <a href="{{ url('password/reset') }}" > Forgot my password </a> </p>

          {!! Form::close() !!}
        </div>
    </div>
@endsection
