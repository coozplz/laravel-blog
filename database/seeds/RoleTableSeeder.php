<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->truncate();


        Role::create([
            'id' => 1,
            'name' => 'Root',
            'description' => 'Use this account with extreme caution'
        ]);


        Role::create([
            'id' => 2,
            'name' => 'administrator',
            'description' => 'Full access to create, edit'
        ]);


        Role::create([
            'id' => 3,
            'name' => 'teacher',
            'description' => 'teacher account'
        ]);


        Role::create([
            'id' => 4,
            'name' => 'student',
            'description' => 'student account'
        ]);
    }
}
