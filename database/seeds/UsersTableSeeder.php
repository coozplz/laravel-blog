<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([

            'name' => 'test',
            'email' => 'test@venturebridge.co.kr',
            'password' => bcrypt('test')
        ]);

        DB::table('users')->insert([
            'name' => 'kwon',
            'email' => 'coozplz@gmail.com',
            'password' => bcrypt('1234')
        ]);

    }
}
