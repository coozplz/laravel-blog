<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->insert([
            'name' => 'teacher',
            'email' => 'teacher@test.com',
            'password' => bcrypt('1234')
        ]);

        DB::table('users')->insert([
            'name' => 'student',
            'email' => 'student@test.com',
            'password' => bcrypt('1234')
        ]);


        DB::table('users')->where('name', 'teacher')->update(['role_id' => 3]);
        DB::table('users')->where('name', 'student')->update(['role_id' => 4]);
    }
}
