<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RoleIdToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function(Blueprint $table) {
            $table->integer('role_id')->after('password')->unsigned()->default(4);

            // 여기서 하는 Foreign key 는 데이터베이스와 관계 없이 Laravel 레벨에서만 하는 것이기 때문에
            // 데이터베이스가 지원하는지 않하는지 알 수 없다..
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {

            $table->dropForeign('users_role_id_foreign');
            // $table->dropForeign('role_id');
            $table->dropColumn('role_id');

        });
        //
    }
}
