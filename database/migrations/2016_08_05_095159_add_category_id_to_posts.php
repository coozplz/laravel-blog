<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToPosts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('posts', function(Blueprint $table) {
            $table->integer('category_id')->nullable()->after('slug')->unsigned();

            // 여기서 하는 Foreign key 는 데이터베이스와 관계 없이 Laravel 레벨에서만 하는 것이기 때문에
            // 데이터베이스가 지원하는지 않하는지 알 수 없다..
            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function(Blueprint $table) {
            $table->dropColumn('category_id');
        });
    }
}
