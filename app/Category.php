<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    // 만약 테이블명이 다를 경우에는 아래와 같이 명시하면 된다..
    protected $table = 'categories';


    public function posts() {
        return $this->hasMany('App\Post');
    }
}
