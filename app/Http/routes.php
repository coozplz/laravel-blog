<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// 사용자 인증
Route::get('auth/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout'] );



// 사용자 등록 처리
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


//패스워드 초기화
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');


// Categories
Route::resource('categories', 'CategoryController', ['except' => ['create']]);


// Tags
Route::resource('tags', 'TagController', ['except' => ['create']]);

//
// 정규 표현식을 이용하여 URL을 사전 검증할 수 있다..
// slug 의 경우 숫자와 문자만 '-', '_' 만 허용되기 때문에 아래와 같이 정규표현식으로 검증한다.
//
Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');


Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);
Route::get('contact', 'PageController@getContact');
Route::get('about', 'PageController@getAbout');
Route::get('/', 'PageController@getIndex');
Route::resource('posts', 'PostController');


Route::get('teacher', ['middleware' => ['auth', 'roles'], 'uses' => 'TestController@getTeacher', 'roles' => ['administrator', 'teacher']]);


Route::get('student', ['middleware' => ['auth', 'roles'], 'uses' => 'TestController@getStudent', 'roles' => ['administrator', 'teacher', 'student']]);
