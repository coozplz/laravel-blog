<?php namespace App\Http\Middleware;


use Closure;


class CheckRole {

    public function handle($request, Closure $next)
    {
        // 요청정보에서 권한을 찾는다.
        $roles = $this->getRequiredRoleForRoute($request->route());


        // 권한이 있다면 라우팅을 한다.
        if ($request->user()->hasRole($roles) || !$roles) {

            return $next($request);
        }


        return response([
            'error' => [
                'code' => 'INSUFFICIENT_ROLE',
                'description' => 'You are not authorized to access this resource'
            ]
        ], 401);
    }


    private function getRequiredroleforroute($route)
    {
        $actions = $route->getAction();
        return isset($actions['roles']) ? $actions['roles'] : null;
    }


}
