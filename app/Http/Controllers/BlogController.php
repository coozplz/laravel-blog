<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;

class BlogController extends Controller
{

    public function getIndex() {
        $posts = Post::paginate(10);
        return view('blog.index')->with('posts', $posts);
    }

    public function getSingle($slug) {

        //
        // slug를 데이터베이스에 unique 타입으로 지정했기 때문에 get() 대신에 first()를 이용한다..
        //
        $post = Post::where('slug', '=', $slug)->first();

        return view('blog.single')->with('post', $post);

    }
}
