<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use App\Post;
use App\Tag;

use Session;
use Log;


class PostController extends Controller
{


    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $posts = Post::orderby('id', 'desc')->paginate(10);
        // $posts = Post::paginate(5);
        // $posts = Post::all();

        foreach ($posts as $post) {
            Log::info($post->id . ' ' . $post->body);
        }
        return view('pages.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('pages.create')->with('categories', $categories)->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, array(
            'title' => 'required|max:255',
            'body' => 'required',
            'category_id' => 'integer',
            'slug' => 'required|max:255|min:5|alpha_dash|unique:posts,slug'
        ));

        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->slug = $request->slug;
        $post->category_id = $request->category_id;

        $post->save();

        // 태그와 게시물의 관계를 형성한다.
        $post->tags()->sync($request->tags, false);

        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);


        return view('pages.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        $categories = Category::all();
        $categoryMap = array();
        foreach ($categories as $category) {
            $categoryMap[$category->id] = $category->name;
        }

        $tags = Tag::all();
        $tagMap = array();

        foreach ($tags as $tag) {
            Log::info('edit: ' . $tag->id . ' ' . $tag->name);
            $tagMap[$tag->id] = $tag->name;
        }
        return view('pages.edit')->withPost($post)->withCategories($categoryMap)->withTags($tagMap);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $post = Post::find($id);

        Log::info("Update: " . $post->slug . ' ' . $request->input('slug'));
        if ($request->input('slug') == $post->slug) {
            $this->validate($request, array(
                'title' => 'required|max:255',
                'body' => 'required'
            ));
        } else {
            $this->validate($request, array(
                'title' => 'required|max:255',
                'slug' => 'required|min:5|max:255|alpha_dash|unique:posts,slug',
                'body' => 'required'
            ));

        }
        // Unique를 늦게 체크하는 이유는 순서대로 검증을 하기 때문에 앞선 검증이 완료되지 않는 경우
        // 데이터베이스 조회를 하지 않기 때문에 순서에 주의해야 한다.
        $post->title = $request->input('title');
        $post->slug = $request->input('slug');
        $post->body = $request->input('body');
        $post->category_id = $request->input('category_id');
        $post->save();


        // sync 함수의 파라미터가 없는 경우 관계를 먼저 삭제하고
        // 이후 관계를 신규로 만든다..
        // 만약 tags 모두 삭제하는 경우 $request->tags 값이 없기 때문에 오류가 발생한다.

        if (isset($request->tags)) {
            $post->tags()->sync($request->tags);
        } else {
            $post->tags()->sync(array());
        }


        Session::flash('success', 'This post was successfully saved...');

        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        // 게시물과 관련된 모든 태그를 제거한다.
        $post->tags()->detach();

        $post->delete();
        Session::flash('success', 'The post was successfully deleted..');
        return redirect()->route('posts.index');
    }
}
