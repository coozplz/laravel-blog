<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestController extends Controller
{

    public function getTeacher()
    {
        return view('pages.teacher');
    }


    public function getStudent()
    {
        return view('pages.student');
    }
}
