<?php

namespace App\Http\Controllers;

use App\Post;

class PageController extends Controller {


    public function getIndex() {

        $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();
        return view('pages.welcome')->with('posts', $posts);
    }


    public function getAbout() {
        $first = 'Ohsung';
        $last = 'Kwon';

        $full = $first . " " . $last;

        $data = [];

        $data['email'] = 'oskwon@venturebridge.co.kr';
        $data['fullname'] = $full;

        return view('pages.about')->withData($data);
    }


    public function getContact() {
        return view('pages.contact');
    }

}
